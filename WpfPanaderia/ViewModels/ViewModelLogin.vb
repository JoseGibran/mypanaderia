﻿Imports References
Imports Services
Namespace ViewModels
    Public Class ViewModelLogin
        Inherits ViewModelBase
        Implements ICommand
        Private _userName As String
        Private _password As String
        Private _VML As ViewModelLogin

        Private _notLoged As String

        Sub New()
            VML = Me
        End Sub

#Region "Propiedades"
        Public Property UserName As String
            Get
                Return _userName
            End Get
            Set(value As String)
                _userName = value
                OnPropertyChanged("UserName")
            End Set
        End Property
        Public Property Password As String
            Get
                Return _password
            End Get
            Set(value As String)
                _password = value
                OnPropertyChanged("Password")
            End Set
        End Property
        Public Property VML As ViewModelLogin
            Get
                Return _VML
            End Get
            Set(value As ViewModelLogin)
                _VML = value
                OnPropertyChanged("VML")
            End Set
        End Property

#End Region
        Public Property NotLoged As String
            Get
                Return _notLoged
            End Get
            Set(value As String)
                _notLoged = value
                OnPropertyChanged("NotLoged")
            End Set
        End Property


        Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
            Return True
        End Function

        Public Event CanExecuteChanged(sender As Object, e As EventArgs) Implements ICommand.CanExecuteChanged

        Public Sub Execute(parameter As Object) Implements ICommand.Execute
            If parameter.ToString() = "Logear" Then
                If UserServices.Login(UserName, Password) Then
                Else
                    NotLoged = "User or Pass are Incorrect"
                End If
            End If
        End Sub
    End Class


End Namespace
