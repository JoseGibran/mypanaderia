﻿Imports Model
Imports References
Namespace Services
    Public Class UserServices
        Private Shared _logedUser As Usuario
        Public Shared Property LogedUser As Usuario
            Get
                Return _logedUser
            End Get
            Set(value As Usuario)
                _logedUser = value
            End Set
        End Property
        Public Shared Function GetUser(ByVal userName As String)
            Try
                Dim users = (From user In DataContext.DBEntities.Usuario Where user.nombreUsuario = userName).FirstOrDefault
                Return users
            Catch ex As Exception
                Throw New Exception(ex.Message, ex.InnerException)
            End Try
        End Function

        Public Shared Function Login(ByVal userName As String, ByVal password As String) As Boolean
            userName = Trim(userName)
            password = Trim(password)

            Dim tryLogedUser As Usuario = GetUser(userName)
            If Not tryLogedUser Is Nothing Then
                If Crypto.Verify(password, tryLogedUser.pass) Then
                    LogedUser = tryLogedUser
                    Return True
                End If
            End If
            Return False
        End Function

        Public Sub Logout()
            LogedUser = Nothing
        End Sub

    End Class
End Namespace
