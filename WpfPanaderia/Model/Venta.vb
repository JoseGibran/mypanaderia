'------------------------------------------------------------------------------
' <auto-generated>
'    Este código se generó a partir de una plantilla.
'
'    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
'    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class Venta
    Public Property idVenta As Integer
    Public Property fecha As Date
    Public Property idPedido As Integer
    Public Property idCaja As Integer

    Public Overridable Property Caja As Caja
    Public Overridable Property Pedido As Pedido

End Class
