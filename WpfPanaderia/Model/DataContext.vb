﻿Namespace Model
    Class DataContext
        Private Shared _dbInstancia As dbPanaderiaEntities
        Public Shared Property DBEntities As dbPanaderiaEntities
            Get
                If _dbInstancia Is Nothing Then
                    _dbInstancia = New dbPanaderiaEntities
                End If
                Return _dbInstancia
            End Get
            Set(value As dbPanaderiaEntities)
                _dbInstancia = value
            End Set
        End Property
    End Class
End Namespace
